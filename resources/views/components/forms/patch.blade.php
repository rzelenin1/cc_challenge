<form method="post" {{ $attributes->merge(['action' => '#', 'class' => '', 'enctype' => 'application/x-www-form-urlencoded']) }}>
    @csrf
    @method('patch')

    {{ $slot }}
</form>
