<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create Email') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">

                <x-forms.post :action="route('email.store')" enctype="multipart/form-data" class="w-full">
                    <div class="md:flex md:items-center mb-6">
                        <div class="md:w-1/3">
                            <label class="block text-gray-500 font-bold text-right mb-1 md:mb-0 pr-4" for="email">
                                Email address
                            </label>
                        </div>
                        <div class="md:w-2/3">
                            <input class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                                   id="email"
                                   name="email"
                                   type="text"
                                   placeholder="{{ __('Email address ') }}"
                                   maxlength="150">
                        </div>
                    </div>

                    <div class="md:flex md:items-center mb-6">
                        <div class="md:w-1/3">
                            <label class="block text-gray-500 font-bold text-right mb-1 md:mb-0 pr-4" for="message">Message</label>
                        </div>
                        <div class="md:w-2/3">
                            <textarea class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                                   id="message"
                                   name="message"
                                   placeholder="{{ __('Message ') }}"
                                   rows="5"></textarea>
                        </div>
                    </div>

                    <div class="md:flex md:items-center mb-6">
                        <div class="md:w-1/3">
                            <label class="block text-gray-500 font-bold text-right mb-1 md:mb-0 pr-4" for="attachment">Attachment</label>
                        </div>

                        <div class="md:w-2/3">
                            <div class="needsclick dropzone" id="file-dropzone"></div>
                            <p class="text-gray-500 text-xs italic">Attachment max size 5Mb.</p>
                        </div>
                    </div>

                    <div class="flex items-center justify-between">
                        <div class="md:w-1/3"></div>
                        <div class="md:w-2/3">
                            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                                Create Email
                            </button>
                        </div>
                    </div>
                </x-forms.post>

            </div>


            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <div class="max-w-xl">
                    <a href="{{ route('email.bulk.store') }}" class="btn btn-secondary">{{ __('Create Bulk Email [JSON]') }}</a>
                </div>
            </div>

        </div>
    </div>

    <x-slot name="scripts">
        <script>
            var uploadedDocumentMap = {}
            Dropzone.options.fileDropzone = {
                url: route('projects.storeMedia'),
                maxFilesize: 50, // MB  // Mailtrap allow max 5Mb. But in test purposes lets add more to get failed job exception (and set email status = 0)
                addRemoveLinks: true,
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                },
                success: function (file, response) {
                    $('form').append('<input type="hidden" name="email_file[]" value="' + response.name + '">')
                    uploadedDocumentMap[file.name] = response.name
                },
                removedfile: function (file) {
                    file.previewElement.remove()
                    var name = ''
                    if (typeof file.file_name !== 'undefined') {
                        name = file.file_name
                    } else {
                        name = uploadedDocumentMap[file.name]
                    }
                    $('form').find('input[name="email_file[]"][value="' + name + '"]').remove()
                },
                init: function () {
                    if (typeof email_files !== 'undefined') {
                        for (var i in email_files) {
                            var file = email_files[i]
                            this.options.addedfile.call(this, file)

                            file.previewElement.classList.add('dz-complete')
                            $('form').append('<input type="hidden" name="email_file[]" value="' + file.file_name + '">')
                        }
                    }
                }
            }
        </script>
    </x-slot>
</x-app-layout>


