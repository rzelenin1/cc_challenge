<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>


---

## Library / Docs

##### PHP
* [Laravel](https://laravel.com/docs/9.x/)
* [Laravel Breeze](https://laravel.com/docs/9.x/starter-kits#laravel-breeze)
* [Laravel-medialibrary](https://spatie.be/docs/laravel-medialibrary/v9/introduction)
* [Laravel Livewire Tables](https://github.com/rappasoft/laravel-livewire-tables)

##### JS
* [Ziggy: A Package for Named Laravel Routes in JavaScript](https://github.com/tightenco/ziggy)
* [Dropzone.js](https://www.dropzone.dev/)

---

## App Install
1. install all required software packages, components, libraries, etc.
2. add domain name to 'hosts', add configure 'host' to nginx (https://laravel.com/docs/9.x/deployment)
3. setup DB
4. copy .env.example to .env and write your configure data
5. run appinstall.sh

---

* After installation please register any user http://your_host/register
* "Dashboard" select "Emails" on top right menu http://your_host/email (this will be a datatable with records of the emails being sent or failed)
* "Emails" click "Create New Email" http://your_host/email/create
* "Ctreate Email" You can create separate email in the form or send bulk emails by JSON located in the EmailController
* All emails be queued using database connection
* Mailtrap allows max 5Mb in attachments. But for test purposes, you can  add more for some emails to get failed job exception. Also, it is limited by dropzone.js settings on the page to 50Mb and you cant attach files biger.
* To process queues run: "php artisan queue:work"
* All processed emails can be found in [Mailtrap](https://mailtrap.io/)
* unit tests - "php artisan test --filter=EmailsTest"

Credentials to [Mailtrap](https://mailtrap.io/): \
login: rz.elenin@gmail.com  
pass: v.c44DjNcxfTES2  

---

- I've installed the latest laravel v.9.48
- I've installed breeze starter kit to deal with authentication. It takes minimum time and just to show that usually CRUD operations are better under the authorized user to not let spam in the app. Of course this step could be skipped.
- I've created emails list with Laravel Livewire Tables package integration to search/order/paginate out-of-the-box (and of course for better look)
- on the email creation page I created a simple form. Also in the same page has the integrated laravel media library package and Dropzone.js to deal with different type of attachments.
- to use laravel routes in javascript(dropzone) I've installed ziggy package
- in EmailController I save new email record with connected media(attachments).
- StoreEmailRequest checks fields for required and correct email.
- NewEmailMail creates an email and SendNewEmailJob put it to a queue. Btw NewEmailMail use new Mail methods (started from laravel 9x, but old type still supported as was declared by creators)
- after queues are processed we update each email status (sent or fail)
- all emails are sent to Mailtrap to check correct address/subjects/body/attachments/etc.
- also in the email controller I've created a method to bulk send of email (bulkStore) that accepts json. The challenge document  was not describing how exactly should come json, so I made it simple by variable. It may include or not an attachment in base64 encoding. Next it go by the same email creation procedure and put media to corresponding collection.