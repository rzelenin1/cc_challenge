<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Email extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $guarded = ['id'];

    protected $casts = [
        'status' => 'boolean',
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('email_files')
            ->useDisk('emailFiles');
    }

    public function getFileAttribute()
    {
        return $this->getFirstMedia('email_files');
    }

    public function getFileUrlAttribute()
    {
        return $this->getFirstMediaUrl('email_files');
    }

}
