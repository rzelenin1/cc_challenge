<?php

namespace App\Http\Livewire;

use App\Models\Email;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\BooleanColumn;

class EmailTable extends DataTableComponent
{
    protected $model = Email::class;

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        return [
            Column::make('ID', 'id')
                ->sortable(),
            Column::make('Email', 'email')
                ->searchable()
                ->sortable(),
            Column::make('Message', 'message')
                ->searchable(),
            BooleanColumn::make('Sent', 'status')
                ->searchable(),
            Column::make('Created', 'created_at')
                ->sortable(),
        ];
    }
}