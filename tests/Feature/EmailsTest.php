<?php

namespace Tests\Feature;

use App\Jobs\SendNewEmailJob;
use App\Mail\NewEmailMail;
use App\Models\Email;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class EmailsTest extends TestCase
{
    use RefreshDatabase;

    private User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = $this->createUser();
    }

    public function test_emails_page_contains_empty_table()
    {
        $response = $this
            ->actingAs($this->user)
            ->get('/email');

        $response->assertStatus(200);

        $response->assertSee(__('No items found.'));
    }

    public function test_emails_page_contains_non_empty_table()
    {
        Email::factory()->create();

        $response = $this
            ->actingAs($this->user)
            ->get('/email');

        $response->assertStatus(200);

        $response->assertDontSee(__('No items found.'));
    }

    public function test_create_email_page_is_displayed(): void
    {
        $response = $this
            ->actingAs($this->user)
            ->get('/email/create');

        $response->assertOk();
    }

    public function test_create_email_successful()
    {
        $arrEmail = [
            'email' => 'test@email.com',
            'message' => 'Test message here'
        ];
        $response = $this
            ->actingAs($this->user)
            ->post('/email', $arrEmail);

        $response->assertStatus(302);
        $response->assertRedirect('email');

        $this->assertDatabaseHas('emails', $arrEmail);

        $lastEmail = Email::latest()->first();
        $this->assertEquals($arrEmail['email'], $lastEmail->email);
        $this->assertEquals($arrEmail['message'], $lastEmail->message);
    }

    public function test_send_new_email_job_dispatched()
    {
        Bus::fake();

        $arrEmail = [
            'email' => 'testme@email.com',
            'message' => 'Hello world!'
        ];
        $response = $this
            ->actingAs($this->user)
            ->post('/email', $arrEmail);

        $response->assertStatus(302);
        $response->assertRedirect('email');

        Bus::assertDispatched(SendNewEmailJob::class);
    }

    public function test_send_new_email_job_successful()
    {
        $email = Email::factory()->create();
        $this->assertNull($email->status);

        (new SendNewEmailJob($email))->handle();

        $email->refresh();
        $this->assertNotNull($email->status);
        $this->assertEquals($email->status, 1);
    }

    public function test_new_email_mail_sent_successfully()
    {
        Mail::fake();

        $arrEmail = [
            'email' => 'testme@email.com',
            'message' => 'Hello world!'
        ];
        $response = $this
            ->actingAs($this->user)
            ->post('/email', $arrEmail);

        $response->assertStatus(302);
        $response->assertRedirect('email');

        Mail::assertSent(NewEmailMail::class);
    }

    private function createUser(): User
    {
        return User::factory()->create();
    }
}
