#!/usr/bin/env bash

MYPWD=${PWD}

echo 'pwd:'$PWD
echo 'start  .........'

echo 'composer install'
composer install

echo 'yarn install'
yarn

echo 'app key:generate'
php artisan key:generate

echo 'set chmod 777 for /bootstrap/cache'
chmod 777 -R $MYPWD/bootstrap/cache

echo 'set chmod 755 for /storage'
chmod 755 -R $MYPWD/storage

echo 'db migrate'
php artisan migrate

echo 'seed data'
php artisan db:seed

echo 'create a symbolic link at  public/storage'
php artisan storage:link

composer clear-all
echo 'npm run build'
npm run dev

echo '....... finish'
